<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use App\Jobs\ProcessDataSchool;
use App\Models\District;

class ProcessDataDistrict implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $kode_wilayah;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($kode_wilayah)
    {
        $this->kode_wilayah = $kode_wilayah;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $kecamatan = Http::get('https://dapo.kemdikbud.go.id/rekap/dataSekolah?id_level_wilayah=2&kode_wilayah='.$this->kode_wilayah.'&semester_id=20222');
        foreach($kecamatan->object() as $row){
            $kode_wilayah = str_replace(' ', '', $row->kode_wilayah);
            $row->nama_kecamatan = $row->nama;
            $array = json_decode(json_encode($row), true);
            $district = District::where('nama_kecamatan', $row->nama)->first();

            if ($district === null) {
                $district = District::create($array);
            }
            dispatch(new ProcessDataSchool($kode_wilayah))->onQueue('high');
        }
    }
}