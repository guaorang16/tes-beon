<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\City;
use Illuminate\Support\Facades\Http;
use App\Jobs\ProcessDataDistrict;

class ProcessDataCity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $kode_wilayah;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($kode_wilayah)
    {
        $this->kode_wilayah = $kode_wilayah;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $kota = Http::get('https://dapo.kemdikbud.go.id/rekap/dataSekolah?id_level_wilayah=1&kode_wilayah='.$this->kode_wilayah.'&semester_id=20222');
        foreach($kota->object() as $row){
            $kode_wilayah = str_replace(' ', '', $row->kode_wilayah);
            $row->nama_kota = $row->nama;
            $array = json_decode(json_encode($row), true);
            $city = City::where('nama_kota', $row->nama)->first();

            if ($city === null) {
                $city = City::create($array);
            }
            dispatch(new ProcessDataDistrict($kode_wilayah))->onQueue('high');
        }
    }
}