<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Goutte\Client;
use App\Models\Province;
use App\Models\City;
use Illuminate\Support\Facades\Http;
use App\Jobs\ProcessDataCity;

class ProcessDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $provinsi = Http::get('https://dapo.kemdikbud.go.id/rekap/dataSekolah?id_level_wilayah=0&kode_wilayah=000000&semester_id=20222');
        foreach($provinsi->object() as $row){
            $kode_wilayah = str_replace(' ', '', $row->kode_wilayah);
            $row->nama_provinsi = $row->nama;
            $array = json_decode(json_encode($row), true);
            $province = Province::where('nama_provinsi', $row->nama)->first();
 
            if ($province === null) {
                $province = Province::create($array);
            }
            dispatch(new ProcessDataCity($kode_wilayah))->onQueue('high');
        }
    }
}