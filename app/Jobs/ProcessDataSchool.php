<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use App\Models\School;

class ProcessDataSchool implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $kode_wilayah;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($kode_wilayah)
    {
        $this->kode_wilayah = $kode_wilayah;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sekolah = Http::get('https://dapo.kemdikbud.go.id/rekap/progresSP?id_level_wilayah=3&kode_wilayah='.$this->kode_wilayah.'&semester_id=20222');
        foreach($sekolah->object() as $row){
            $row->nama_sekolah = $row->nama;
            $array = json_decode(json_encode($row), true);
            $school = School::where('nama_sekolah', $row->nama)->first();

            if ($school === null) {
                $school = School::create($array);
            }
        }
    }
}
