<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;
    protected $fillable = ['sekolah_id', 'nama_sekolah', 'npsn', 'induk_kecamatan', 'kode_wilayah_induk_kecamatan', 'induk_kabupaten', 'kode_wilayah_induk_kabupaten', 'induk_provinsi', 'kode_wilayah_induk_provinsi', 'bentuk_pendidikan', 'status_sekolah', 'ptk', 'pegawai', 'pd', 'rombel', 'jml_rk', 'jml_lab', 'jml_perpus'];
}
