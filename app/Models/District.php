<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;
    protected $fillable = ['kode_wilayah', 'nama_kecamatan', 'induk_kabupaten', 'kode_wilayah_induk_kabupaten', 'induk_provinsi', 'kode_wilayah_induk_provinsi', 'tk_n', 'tk_s', 'kb_n', 'kb_s', 'tpa_n', 'tpa_s', 'sps_n', 'sps_s', 'pkbm_n', 'pkbm_s', 'skb_n', 'skb_s', 'sd_n', 'sd_s', 'smp_n', 'smp_s', 'sma_n', 'sma_s', 'smk_n', 'smk_s', 'slb_n', 'slb_s', 'sekolah_n', 'sekolah_s'];
}