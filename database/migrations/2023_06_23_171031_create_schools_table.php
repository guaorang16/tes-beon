<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->id();
            $table->string('sekolah_id');
            $table->string('nama_sekolah');
            $table->string('induk_kecamatan');
            $table->string('kode_wilayah_induk_kecamatan');
            $table->string('induk_kabupaten');
            $table->string('kode_wilayah_induk_kabupaten');
            $table->string('induk_provinsi');
            $table->string('kode_wilayah_induk_provinsi');
            $table->string('bentuk_pendidikan');
            $table->string('status_sekolah');
            $table->string('npsn');
            $table->integer('ptk');
            $table->integer('pegawai');
            $table->integer('pd');
            $table->integer('rombel');
            $table->integer('jml_rk');
            $table->integer('jml_lab');
            $table->integer('jml_perpus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
};
