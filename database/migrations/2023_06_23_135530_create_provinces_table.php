<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->id();
            $table->string('kode_wilayah');
            $table->string('nama_provinsi');
            $table->integer('tk_n');
            $table->integer('tk_s');
            $table->integer('kb_n');
            $table->integer('kb_s');
            $table->integer('tpa_n');
            $table->integer('tpa_s');
            $table->integer('sps_n');
            $table->integer('sps_s');
            $table->integer('pkbm_n');
            $table->integer('pkbm_s');
            $table->integer('skb_n');
            $table->integer('skb_s');
            $table->integer('sd_n');
            $table->integer('sd_s');
            $table->integer('smp_n');
            $table->integer('smp_s');
            $table->integer('sma_n');
            $table->integer('sma_s');
            $table->integer('smk_n');
            $table->integer('smk_s');
            $table->integer('slb_n');
            $table->integer('slb_s');
            $table->integer('sekolah_n');
            $table->integer('sekolah_s');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinces');
    }
};
