@extends('admin')

@section('title', (!empty($id)? 'Scrap' : 'Scrap') . ' Data')

@section('content')

<h3 class="well">@yield('title')</h3>

<div id="container">
    <form>
        @csrf
        <table style="border:0; width:100%;">
            <tr>
                <td>Periode :</td>
                <td>
                    <input type="text" name="description" value="" class="btn btn-default" />
                </td>
            </tr>
        </table>
    </form>
    <div id="container">
</div>
    <a class="btn btn-primary pull-right" href="{{ url()->previous() }}" class="btn btn-primary">Go Back</a>
</div>

@endsection

@section('javascript')
@parent

<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<link href="{{ url('/'); }}/public/assets/admin/toastr.min.css" rel="stylesheet" />
<script src="{{ url('/'); }}/public/assets/admin/toastr.min.js"></script>

<script src="{{ url('/'); }}/public/assets/admin/fileupload.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        initDataTable('table_data', '{{ url('/list_order'); }}', 'undefined', 'undefined', [], [0, 'ASC']);
    });
</script>

@endsection

@section('stylesheet')
@parent

<style>
    td {
        padding-right: 15px;
        padding-bottom: 10px;
    }
    input[type=text], select, textarea {
        text-align: left !important;
        width: 100%;
    }
    .required:after {
        content: '*';
        color: red;
        margin-left: 5px;
    }
</style>

@endsection