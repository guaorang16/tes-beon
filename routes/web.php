<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Scrap;
use Goutte\Client;
use App\Jobs\ProcessDataJob;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Artisan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/scrape', function () {
	dispatch(new ProcessDataJob())->onQueue('high');
});

Route::any('/scrap', [Scrap::class, 'list']);

Route::post('/user/{user}/mail', function ($user) {
    $exitCode = Artisan::call('mail:send', [
        'user' => $user, '--queue' => 'default'
    ]);
});

Route::get('/', function () {
	return view('admin/welcome');
});